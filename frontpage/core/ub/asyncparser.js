/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

 (function () {
    OpenBlock.AsyncParser = {};

    /**
     * 异步分析的setTimeout
     */
    OpenBlock.AsyncParser.TimeoutHandler = 0;

    /**
     * 分析的延迟
     */
    OpenBlock.AsyncParser.WaitingTime = 10000;
    OpenBlock.AsyncParser.ParseNow = function (workspaceId) {
        clearTimeout(OpenBlock.AsyncParser.TimeoutHandler);
        OpenBlock.AsyncParser.TimeoutHandler = 0;
        var workspace = Blockly.Workspace.getById(workspaceId);
        if(workspace!=null){
            OpenBlock.BlocklyParser.parseFromWorkspace(workspace);
        }
    };
    OpenBlock.AsyncParser.SetTimeout = function (workspaceId) {
        clearTimeout(OpenBlock.AsyncParser.TimeoutHandler);
        OpenBlock.AsyncParser.TimeoutHandler =
            setTimeout(OpenBlock.AsyncParser.ParseNow, OpenBlock.AsyncParser.WaitingTime, workspaceId);
    };
    // OpenBlock.Blocks.EventBaseListener = function (event) {
    //     if (event.type === Blockly.Events.BLOCK_MOVE ||
    //         event.type === Blockly.Events.BLOCK_CREATE ||
    //         event.type === Blockly.Events.BLOCK_CHANGE||
    //         event.type === Blockly.Events.VAR_CREATE||
    //         event.type === Blockly.Events.VAR_RENAME||
    //         event.type === Blockly.Events.MARKER_MOVE) {
    //             OpenBlock.AsyncParser.SetTimeout(event.workspaceId);
    //     }
    // };
    // OpenBlock.wsBuildCbs.push(function (workspace) {
    //     workspace.addChangeListener(OpenBlock.Blocks.EventBaseListener);
    // });

})();