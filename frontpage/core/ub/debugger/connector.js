/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
class OBConnector {
    /**
     * @type {String}
     */
    name;
    // /**
    //  * 子类中实现，connector中不需要实现
    //  * @type {Function}
    //  */
    // config;
    /**
     * run project
     */
    runProject() { }
    /**
     * start debug project
     * @returns {OBDebugContext}
     */
    debugProject() { }
    /**
     * deploy 
     */
    // deploy() { }
    /**
     * package
     */
    // package() { }
    loadConfig() { }

    // openConfigUI() { }
    constructor() {
        this.name = this.constructor.name;
    }
}
class OBDebugContext {

}
