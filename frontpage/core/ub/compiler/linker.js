/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */

class Linker {
    static worker;
    linked;
    onFinisheds = [];
    link(compiledLibs, data, nativeFuncMap, cb) {
        this.stop();
        Linker.worker = new WorkerContext(OpenBlock.bootPath + 'compiler/linkerWorker.js');
        Linker.worker.postMessage({ compiledLibs, data, nativeFuncMap }, (e, r) => {
            if (!e) {
                OpenBlock.Linker.linked = r;
            }
            if (cb) {
                cb(e, r);
            }
            for (let i = 0; i < this.onFinisheds.length; i++) {
                let f = this.onFinisheds[i];
                try {
                    f(e, r);
                } catch (e) {
                    console.error(e);
                }
            }
            // Linker.worker.terminate();
        });
    }
    onFinished(f) {
        this.onFinisheds.push(f);
    }
    stop() {
        if (Linker.worker) {
            Linker.worker.terminate();
        }
    }
}
OpenBlock.Linker = new Linker();