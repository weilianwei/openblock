/**
 * @license
 * Copyright 2021 Du Tian Wei
 * SPDX-License-Identifier: Apache-2.0
 */
export class Vector2 {
    x; y;
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
export class OBCanvas2D {
    /**
     * @type {HTMLCanvasElement}
     */
    canvas;
    /**
     * @type {CanvasRenderingContext2D}
     */
    canvas2dctx;
    /**
     * 
     * @param {HTMLCanvasElement} canvas 
     */
    constructor(canvas) {
        this.canvas = canvas;
        this.canvas2dctx = canvas.getContext('2d');
    }
    setFillStyleColor(color) {
        let str_color;
        if (color < 0) {
            str_color = (Number.MAX_SAFE_INTEGER + color + 1).toString(16).substr(-8)
            str_color = '#' + str_color;
        } else {
            str_color = '#' + color.toString(16).padStart(8, '0');
        }
        this.canvas2dctx.fillStyle = str_color;
    }
    setStrokeStyleColor(color) {
        let str_color;
        if (color < 0) {
            str_color = (Number.MAX_SAFE_INTEGER + color + 1).toString(16).substr(-8)
            str_color = '#' + str_color;
        } else {
            str_color = '#' + color.toString(16).padStart(8, '0');
        }
        this.canvas2dctx.strokeStyle = str_color;
    }
    getVector2X(v) {
        return v.x;
    }
    getVector2Y(v) {
        return v.y;
    }
    /**
     * 安装到脚本库
     * @param {OBScript} script 
     */
    install(script) {
        script.InstallLib("canvas2d", "canvas2d", [
            script.NativeUtil.closureVoid(this.canvas2dctx.fillRect.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.clearRect.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            // canvas2dctx.fillStyle=color
            script.NativeUtil.closureVoid(this.setFillStyleColor.bind(this), ['LongRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.strokeRect.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.fillText.bind(this.canvas2dctx), ['StringRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.beginPath.bind(this.canvas2dctx), []),
            script.NativeUtil.closureVoid(this.canvas2dctx.arc.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.fill.bind(this.canvas2dctx), []),
            script.NativeUtil.closureVoid(this.canvas2dctx.closePath.bind(this.canvas2dctx), []),
            script.NativeUtil.fieldSetter(this.canvas2dctx, 'font', 'StringRegister'),
            script.NativeUtil.fieldGetter(this.canvas2dctx, 'font', 'StringRegister'),
            script.NativeUtil.closureVoid(this.canvas2dctx.arcTo.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.bezierCurveTo.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.moveTo.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.lineTo.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.fieldSetter(this.canvas2dctx, 'textAlign', 'StringRegister'),
            script.NativeUtil.fieldGetter(this.canvas2dctx, 'textAlign', 'StringRegister'),
            script.NativeUtil.fieldSetter(this.canvas2dctx, 'textBaseline', 'StringRegister'),
            script.NativeUtil.fieldGetter(this.canvas2dctx, 'textBaseline', 'StringRegister'),
            script.NativeUtil.closureVoid(this.canvas2dctx.ellipse.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'LongRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.rect.bind(this.canvas2dctx), ['DoubleRegister', 'DoubleRegister', 'DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.rotate.bind(this), ['DoubleRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.scale.bind(this), ['DoubleRegister', 'DoubleRegister']),
            script.NativeUtil.closureVoid(this.setStrokeStyleColor.bind(this), ['LongRegister']),
            script.NativeUtil.closureVoid(this.canvas2dctx.stroke.bind(this.canvas2dctx), []),
            script.NativeUtil.objFieldGetter('x', 'DoubleRegister'),
            script.NativeUtil.objFieldGetter('y', 'DoubleRegister'),
        ]);
    }
}